#define _CRT_SECURE_NO_WARNINGS
#include"StudentInfo.h"

//创建头节点
LNode* LNodeInit()
{
	LNode* guard = (LNode*)malloc(sizeof(LNode));  //设置一个头节点
	if (guard == NULL)
	{
		perror("LNodeInit malloc fail!");
		exit(-1);
	}
	guard->next = NULL;
	return guard;
}


//销毁链表
void LnodeDestory(LNode* slist)
{
	assert(slist);
	LNode* cur = slist;
	while (cur)
	{
		LNode* next = cur->next;  //保存下一个节点的地址
		free(cur);   //释放当前节点
		cur = next;  //迭代
	}
}

//调用输入学生信息函数
void InputStuInfo(LNode* newnode)
{
	printf("请输入学生的姓名：");
	scanf("%s", newnode->data.name);

	printf("请输入学生的学号：");
	scanf("%s", newnode->data.StudentNumber);


	printf("请输入学生的性别：");
	scanf("%s", newnode->data.sex);

	printf("请输入学生的年龄：");
	scanf("%s", newnode->data.age);

	printf("请输入学生的专业：");
	scanf("%s", newnode->data.major);

	printf("请输入学生的年级：");
	scanf("%s", newnode->data.grade);

	printf("请输入学生的班级：");
	scanf("%s", newnode->data.classes);

	printf("请输入学生的成绩：");
	scanf("%lf", &newnode->data.score);
	printf("\n");
}
//增加节点函数
LNode* NodeCreate()
{
	LNode* newnode = (LNode*)malloc(sizeof(LNode));  //创建新节点
	if (newnode == NULL)
	{
		perror("LnodeCreate malloc fail");
		exit(-1);
	}
	newnode->next = NULL;
	return newnode;
}
//增加学生信息
void LnodeCreate(LNode* slist)
{
	assert(slist);
	LNode* newnode = NodeCreate(); //增加一个新节点
	LNode* cur = slist;
	while (cur->next != NULL)  //找到尾节点（设计一个带尾指针的）
	{
		cur = cur->next;
	}
	cur->next = newnode;
	InputStuInfo(newnode);  //学生信息输入
}

//打印学生信息
void LnodePrint(LNode* slist)
{
	assert(slist);
	LNode* cur = slist->next;  //找到首元节点
	if (cur == NULL)
	{
		printf("暂无任何学生信息,无法显示！\n");
		return;
	}
	printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-2s\n", "姓名", "学号", "性别", "年龄", "专业", "年级", "班级", "成绩");
	while (cur != NULL)
	{
		printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-.2lf",
			cur->data.name,
			cur->data.StudentNumber,
			cur->data.sex,
			cur->data.age,
			cur->data.major,
			cur->data.grade,
			cur->data.classes,
			cur->data.score);
		printf("\n");
		cur = cur->next;
	}
}

//根据学号删除学生信息
void LnodePopByStudentNumber(LNode* slist)
{
	assert(slist);
	if (slist->next == NULL)
	{
		printf("暂无任何学生信息，无法进行删除\n");
		return;
	}
	char stunumber[15];
	printf("请输入需要删除学生信息的学号：");
	scanf("%s", stunumber);
	LNode* cur = slist->next;
	LNode* prev = slist;  //保存该节点的前一个节点地址
	while (cur != NULL)
	{
		if (strcmp(cur->data.StudentNumber, stunumber) == 0)
		{
			prev->next = cur->next;  //链接cur节点前后的两个节点
			free(cur); //释放对应的节点
			break;
		}
		prev = cur;
		cur = cur->next; //迭代
	}
}

//根据学号查找学生信息
void StuInfoNumberFind(LNode* slist)
{
	assert(slist);
	if (slist->next == NULL)
	{
		printf("暂无任何学生信息，无法查找\n");
		return;
	}
	char stunumber[15];
	printf("请输入需要查找学生信息的学号：");
	scanf("%s", stunumber);
	LNode* cur = slist->next;
	while (cur != NULL)
	{
		if (strcmp(cur->data.StudentNumber, stunumber) == 0)
		{
			printf("查找到的学生信息如下:\n");
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-2s\n", "姓名", "学号", "性别", "年龄", "专业", "年级", "班级", "成绩");
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-.2lf",
				cur->data.name,
				cur->data.StudentNumber,
				cur->data.sex,
				cur->data.age,
				cur->data.major,
				cur->data.grade,
				cur->data.classes,
				cur->data.score);
			printf("\n");
			break;
		}
		cur = cur->next;
	}
	if (cur == NULL)
	{
		printf("系统中不存在此学号，请仔细核对！\n");
	}
}

//根据班级查找学生信息
void StuInfoClassFind(LNode* slist)
{
	assert(slist);
	if (slist->next == NULL)
	{
		printf("暂无任何学生信息，无法查找\n");
		return;
	}
	int count = 0;  //用来记录该信息表中是否存在该班的学生
	char classes[6];
	printf("请输入需要查找学生信息的班级：");
	scanf("%s", classes);
	LNode* cur = slist->next;
	while (cur != NULL)
	{
		if (strcmp(cur->data.classes, classes) == 0)
		{
			printf("该班的第%d个学生的信息如下：\n",count+1);
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-2s\n", "姓名", "学号", "性别", "年龄", "专业", "年级", "班级", "成绩");
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-.2lf",
				cur->data.name,
				cur->data.StudentNumber,
				cur->data.sex,
				cur->data.age,
				cur->data.major,
				cur->data.grade,
				cur->data.classes,
				cur->data.score);
			printf("\n");
			count++;
		}
		cur = cur->next;
	}
	if (0 == count)
	{
		printf("系统中不存在该班学生的信息，请仔细核对！\n");

	}
}

//根据姓名查找学生信息
void StuInfoNameFind(LNode* slist)
{
	assert(slist);
	if (slist->next == NULL)
	{
		printf("暂无任何学生信息，无法查找\n");
		return;
	}
	char name[20];
	printf("请输入需要查找学生信息的姓名：");
	scanf("%s", name);
	LNode* cur = slist->next;
	while (cur != NULL)
	{
		if (strcmp(cur->data.name, name) == 0)
		{
			printf("学生%s的信息如下\n", name);
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-2s\n", "姓名", "学号", "性别", "年龄", "专业", "年级", "班级", "成绩");
			printf("%-10s\t%-15s\t%-5s\t%-4s\t%-15s\t%-10s\t%-6s\t%-.2lf",
				cur->data.name,
				cur->data.StudentNumber,
				cur->data.sex,
				cur->data.age,
				cur->data.major,
				cur->data.grade,
				cur->data.classes,
				cur->data.score);
			printf("\n");
			break;
		}
		cur = cur->next;
	}
	if (cur == NULL)
	{
		printf("系统中不存在%s的信息，请仔细核对！\n",name);
	}
}

//根据学号进行修改学生的信息
void StuInfoModifyByNumber(LNode* slist)
{
	assert(slist);
	if (slist->next == NULL)
	{
		printf("暂无任何学生信息，无法进行修改\n");
		return;
	}
	char stunumber[15];
	printf("请输入需要修改学生信息的学号：");
	scanf("%s", stunumber);
	LNode* cur = slist->next;
	while (cur != NULL)
	{
		if (strcmp(cur->data.StudentNumber, stunumber) == 0)
		{
			printf("请重新输入%s的信息\n",cur->data.name);
			InputStuInfo(cur);
			break;
		}
		cur = cur->next;
	}
	if (cur == NULL)
	{
		printf("系统中不存在此学号，请仔细核对！\n");
	}
}

//录入学生信息（从文本中录入）
//此函数只是创建节点，数据从文件中导入
void LnodePush(LNode* slist)
{
	assert(slist);
	//打开文件
	FILE* fp;
	fp = fopen("学生信息.txt", "r");
	if (fp == NULL)
	{
		perror("open file fail");
		exit(-1);
	}
	//先创建一个新节点
	LNode* newnode =  NodeCreate();
	//形成链接
	slist->next = newnode;
	LNode* cur = slist->next;
	//录入信息
	while (fscanf(fp, "%s %s %s %s %s %s %s %lf", newnode->data.name, newnode->data.StudentNumber, newnode->data.sex, newnode->data.age, newnode->data.major, newnode->data.grade,
		newnode->data.classes, &newnode->data.score)!= EOF)
	{
		//记录最后一个为节点
		cur = newnode;
		newnode = NodeCreate();   //再创建新的节点

		//形成链接
		cur->next = newnode;
	}
	free(newnode);
	cur->next = NULL;
	fclose(fp);
}
//登录选项设置
//登录选择设置
void title()
{
	printf("**************************************************************\n");
	printf("**************************************************************\n");
	printf("************     欢迎进入重庆师范大学学生信息管理系统*********\n");
	printf("************  1、新生信息入录     2、学生信息删除    *********\n");
	printf("************  3、修改学生信息     4、查看所有学生信息*********\n");
	printf("************  5、查找某位学生     0、退出系统        *********\n");
	printf("**************************************************************\n");
	printf("**************************************************************\n");
}
void menu()
{
	LNode* guard = LNodeInit();  //创建链表的头
	int option = 0;
	do
	{
		title();
		printf("请选择你要进行的操作:");
		scanf("%d", &option);
		switch (option)
		{
		case 0:
			printf("退出系统\n");
			break;

		case 1:
		{
			int option_2 = 0;
			printf("1、从已有文件中入录学生信息\n2、手动入录学生信息\n");
			printf("请选择需要进行的操作:");
			scanf("%d", &option_2);
			switch (option_2)
			{
			case 1:
				LnodePush(guard);  //录入信息
				printf("学生信息入录完成\n");
				break;
			case 2:
				LnodeCreate(guard);  // 输入信息
				printf("学生信息输入成功");
				break;
			default:
				printf("输入选项有误，请重新选择\n");
				break;
			}
		}
		break;
		case 2:
		{
			LnodePopByStudentNumber(guard);
		}
		break;
		case 3:
		{
			StuInfoModifyByNumber(guard);
		}
		break;
		case 4:
			LnodePrint(guard);
			break;
		case 5:
		{
			int option_3 = 0;
			printf("1、学号查找学生信息\n2、班级查找学生信息\n3、姓名查找学生信息\n");
			printf("请选择需要进行的操作:");
			scanf("%d", &option_3);
			switch (option_3)
			{
			case 1:
			{
				StuInfoModifyByNumber(guard);
			}
			break;
			case 2:
			{
				StuInfoClassFind(guard);
			}
			break;
			case 3:
			{
				StuInfoNameFind(guard);
			}
			default:
				printf("输入选项有误，请重新选择\n");
				break;
			}
		}
		break;
		default:
			printf("输入选项有误，请重新选择\n");
			break;
		}

	} while (option);

}