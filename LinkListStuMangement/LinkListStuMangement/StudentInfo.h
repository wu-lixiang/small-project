#pragma once
//头文件包含
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

//宏定义
#define MAX_SIZE_NAME 20  //姓名数组最大容量
#define MAX_SIZE_SEX 5    //性别数组的最大容量
#define MAX_SIZE_MAJOR 30 //专业数组的最大容量
#define MAX_SIZE_STUDENTNUMBER 15 //学号数组最大容量
#define MAX_SIZE_GRADE   10  //年级数组最大容量
#define MAX_SIZE_CLASSES 6   //班级数组最大容量
#define MAX_SIZE_AGE 4       //年龄数组最大容量


//学生信息
typedef struct StuInfo
{
	char name[MAX_SIZE_NAME]; //姓名
	char StudentNumber[MAX_SIZE_STUDENTNUMBER];  //学号
	char sex[MAX_SIZE_SEX];   //性别
	char age[MAX_SIZE_AGE];  //年龄
	char major[MAX_SIZE_MAJOR];  //专业
	char grade[MAX_SIZE_GRADE]; //年级
	char classes[MAX_SIZE_CLASSES]; //班级
	double score;  //成绩
}StInfo;

//带头双向链表
typedef struct LinkNode
{
	StInfo data;
	struct LinkNode* next;
}LNode;

//创建头节点
LNode* LNodeInit();

//销毁链表
void LnodeDestory(LNode* slist);

//录入学生信息（从文本中录入）
void LnodePush(LNode* slist);

//增加学生信息
void LnodeCreate(LNode* slist);

//调用输入学生信息函数
void InputStuInfo(LNode* newnode);

//打印学生信息
void LnodePrint(LNode* slist);

//根据学号删除学生信息
void LnodePopByStudentNumber(LNode* slist);

//根据学号查找学生信息
void StuInfoNumberFind(LNode* slist);

//根据班级查找学生信息
void StuInfoClassFind(LNode* slist);

//根据姓名查找学生信息
void StuInfoNameFind(LNode* slist);

//根据学号进行修改学生的信息
void StuInfoModifyByNumber(LNode* slist);

//增加节点函数
LNode* NodeCreate();

//登录选项设置
void menu();