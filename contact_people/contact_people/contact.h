#pragma once
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>

//创建一个结构体表示通讯录中人的信息
typedef struct PeInfo 
{
	char name[20];  
	int age;
	char tele[12];
	char sex[10];
	char addr[20];
}PeInfo;    //
//创建一个结构体将通讯录中的人的信息和数量进行封装
typedef struct Contact_people
{
	PeInfo message[100];
	int num; //记录当前通讯录中人的个数
}Contact_people;

//初始化函数声明
void Init_Contact(struct Contact_people* pc);

//增添人的信息函数声明
void Add(struct Contact_people* pc);

//显示人的信息函数声明
void Show_Contact(struct Contact_people* pc);

//寻找人的信息函数声明
void Search_Contact(struct Contact_people* pc);

//删除人的信息函数声明
void Del(struct Contact_people* pc);

//修改人的信息函数声明
void Modify_Contact(struct Contact_people* pc);

//排序人的信息
void Sort_Contact(struct Contact_people* pc);