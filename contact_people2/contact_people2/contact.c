#define _CRT_SECURE_NO_WARNINGS
#include "contact.h"


//初始化函数
void Init_Contact(struct Contact_people* pc)
{
	assert(pc); //如果pc为空指针就报错
	pc->num = 0;
	pc->message = (PeInfo*)calloc(CAPACITY, sizeof(PeInfo));  // 因为message为PeoInfo*，要使用void* calloc（num, size）
	//calloc：初始化num个大小为size字节的内存单元
	if (pc->message == NULL)
	{
		printf("Init_Contact::%s\n", strerror(errno));
	}
	pc->capacity = CAPACITY; // 初始化容量
} 

void check(struct Contact_people* pc)
{
	if (pc->capacity == pc->num)
	{
		PeInfo* ptr = (PeInfo*)realloc(pc->message, (pc->capacity + ADD_SIZE) * sizeof(PeInfo)); //开辟内存大小的书写
		if (ptr == NULL)
		{
			printf("增容失败\n");
			printf("Add::%s\n", strerror(errno));
		}
		else
		{
			pc->message = ptr;
			pc->capacity += ADD_SIZE;
			printf("增容成功！\n");
		}
	}
}
//增添人的信息函数
void Add(struct Contact_people* pc)
{
	assert(pc);  //如果pc为空指针就报错
	check(pc);
	printf("请输入要增添人的姓名>:");
	scanf("%s", pc->message[pc->num].name);//注意这里的书写形式，message为一个数组，
	//数组的每个元素为一个结构体，结构体里面放着人的信息
	//存放人信息的结构体访问用 .操作符，因为其不是指针
	printf("请输入要增添人的性别>:");
	scanf("%s", pc->message[pc->num].sex);

	printf("请输入要增添人的年龄>:");
	scanf("%d", &pc->message[pc->num].age);

	printf("请输入要增添人的电话>:");
	scanf("%s", pc->message[pc->num].tele);

	printf("请输入要增添人的地址>:");
	scanf("%s", pc->message[pc->num].addr);
	pc->num += 1;
	printf("增加成功！\n");
}

//显示人的信息函数
void Show_Contact(struct Contact_people* pc)
{
	assert(pc); //pc为空指针就报错
	if (0 == pc->num)
	{
		printf("通讯录为空!\n");
		return;
	}
	int i = 0;
	printf("%-20s\t%-5s\t%-4s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "电话", "地址");
	for (i = 0; i < pc->num; i++)
	{
		printf("%-20s\t%-5s\t%-4d\t%-12s\t%-20s\n", pc->message[i].name,
			pc->message[i].sex,
			pc->message[i].age,
			pc->message[i].tele,
			pc->message[i].addr);
	}

}


int find_name(const struct Contact_people* pc, char name[])
{
	assert(pc);//如果pc为空指针见就报错
	int i = 0;
	for (i = 0; i < pc->num; i++)
	{
		if (strcmp(name, pc->message[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}

//删除人的信息函数
void Del(struct Contact_people* pc)
{
	assert(pc);//如果pc为空指针就报错
	//判断所要删除的人在不在通讯录中
	if (0 == pc->num)
	{
		printf("通讯录为空，无法删除！\n");
		return;
	}
	char name[MAX_PEO_NAME];
	printf("请输入你要删除人的姓名>:");
	scanf("%s", name);
	int ret = find_name(pc, name);
	if (-1 == ret)
	{
		printf("您查找的人不在通讯录中！\n");
	}
	else
	{
		int i = 0;
		for (i = ret; i < pc->num - 1; i++)   //num表示的是数量，所以比下标要大一个	
		{
			pc->message[i] = pc->message[i + 1];
		}
		pc->num -= 1;
		printf("删除成功!\n");
	}


}

//寻找人的信息函数
void Search_Contact(struct Contact_people* pc)
{
	assert(pc); //如果pc为空指针就报错
	if (0 == pc->num)
	{
		printf("通讯录为空，无法查找！\n");
		return;
	}
	char name[MAX_PEO_NAME];
	printf("请输入你要查找的姓名>:");
	scanf("%s", name);
	int ret = find_name(pc, name);
	if (-1 == ret)
	{
		printf("您查找的人不在通讯录中！\n");
	}
	else
	{
		printf("找到了！信息如下!\n");
		printf("%-20s\t%-5s\t%-4s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "电话", "地址");
		printf("%-20s\t%-5s\t%-4d\t%-12s\t%-20s\n", pc->message[ret].name,
			pc->message[ret].sex,
			pc->message[ret].age,
			pc->message[ret].tele,
			pc->message[ret].addr);
	}
}

//修改人的信息函数
void Modify_Contact(struct Contact_people* pc)
{
	assert(pc);  //如果pc为空指针就报错
	if (0 == pc->num)
	{
		printf("通讯录为空，无法查找！\n");
		return;
	}
	char name[MAX_PEO_NAME];
	printf("请输入你要修改人的姓名>:");
	scanf("%s", name);
	int ret = find_name(pc, name);
	if (-1 == ret)
	{
		printf("您要修改的人不在通讯录中！\n");
	}
	else
	{
		printf("请输入修改后的姓名>:");
		scanf("%s", pc->message[ret].name);

		printf("请输入修改后的性别>:");

		scanf("%s", pc->message[ret].sex);

		printf("请输入修改后的年龄>:");
		scanf("%d", &pc->message[ret].age);

		printf("请输入修改后的电话>:");
		scanf("%s", pc->message[ret].tele);

		printf("请输入修改后的地址>:");
		scanf("%s", pc->message[ret].addr);
		printf("修改成功！\n");
	}
}

//排序人的信息（以形式来排）
int cmp_by_name(const void* e1, const void* e2)
{
	return strcmp(((PeInfo*)e1)->name, ((PeInfo*)e2)->name);
}
void Sort_Contact(struct Contact_people* pc)
{
	assert(pc);  //如果pc为空指针就报错
	if (0 == pc->num)
	{
		printf("通讯录为空，无法排序！\n");
		return;
	}
	else
	{
		qsort(pc->message, pc->num, sizeof(PeInfo), cmp_by_name);  //这里的qsort函数需要注意
		printf("排序成功啦！\n");
	}



}




