#pragma once
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>
#include<errno.h>

//将常用的变量统一使用#define来定义
#define MAX_PEO_NAME 20
#define MAX_PEO_TELE 12
#define MAX_PEO_SEX 5
#define MAX_PEO_ADDR 20
#define CAPACITY   3   // 定义刚开始通讯录的容量为3
#define ADD_SIZE 2   //每次扩容的量


//创建一个结构体表示通讯录中人的信息
typedef struct PeInfo
{
	char name[MAX_PEO_NAME];
	int age;
	char tele[MAX_PEO_TELE];
	char sex[MAX_PEO_SEX];
	char addr[MAX_PEO_ADDR ];
}PeInfo;    //
//创建一个结构体将通讯录中的人的信息和数量进行封装
typedef struct Contact_people
{
	PeInfo* message;  //使用结构体指针创建一个指针变量，然后使用malloc开辟空间
	int num;   //记录当前通讯录的人数
	int capacity; //记录当前通讯录的容量
}Contact_people;

//初始化函数声明
void Init_Contact(struct Contact_people* pc);

//增添人的信息函数声明
void Add(struct Contact_people* pc);

//显示人的信息函数声明
void Show_Contact(struct Contact_people* pc);

//寻找人的信息函数声明
void Search_Contact(struct Contact_people* pc);

//删除人的信息函数声明
void Del(struct Contact_people* pc);

//修改人的信息函数声明
void Modify_Contact(struct Contact_people* pc);

//排序人的信息
void Sort_Contact(struct Contact_people* pc);
