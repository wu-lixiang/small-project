#define _CRT_SECURE_NO_WARNINGS
#include "contact.h"


void menu()
{
	printf("*****************************************************\n");
	printf("********    1、add          2、del           ********\n");
	printf("********    3、search       4、modify        ********\n");
	printf("********    5、show         6、sort          ********\n");
	printf("********              0、exit                ********\n");
	printf("*****************************************************\n");
}

int main()
{
	int input = 0;
	Contact_people con; //利用通讯录结构体在主文件中创建一个名为con的通讯录
	Init_Contact(&con);  //进行封装初始化
	do
	{
		menu();
		printf("请选择 >");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("退出通讯录\n");
			break;
		case 1:
			Add(&con);
			break;
		case 2:
			Del(&con);
			break;
		case 3:
			Search_Contact(&con);
			break;
		case 4:
			Modify_Contact(&con);
			break;
		case 5:
			Show_Contact(&con);
			break;
		case 6:
			Sort_Contact(&con);
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	} while (input);
	return 0;
}

