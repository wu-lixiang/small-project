#define _CRT_SECURE_NO_WARNINGS
#include"fun.h"   //引头文件
//初始化棋盘函数
void initial_arr(char arr[ROWS][COLS], int row, int  col,char set)   //为了使函数的功能多样化，再创建一个char  set变量，使该函数可以根据不同需求进行初始化
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			arr[i][j] = set;
		}
	}
}
//打印棋盘函数   
void print_arr(char arr[ROWS][COLS], int row, int col)
{ 
	int i = 0;
	for (i = 0; i <= col; i++)   //让玩家更能明白是哪一列
	{
		printf(" %d ", i);
	}
	printf("\n");
	for (i = 1; i <=row; i++)  //这里的i应该是从1开始的，否则打印的就有问题
	{
		printf(" %d ", i );         //让玩家更能明白是哪一列
		int j = 0;
		for (j = 1; j <= col; j++)
		{
			printf(" %c ",arr[i][j]);
		}
		printf("\n");   //记得换行
	}
}
//设置雷函数
void get_mine(char arr[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int count = MINE;
	while (count)
	{
		x = rand() % 9 + 1;         //这里的rand函数要放在while循环内啊，否则种子无法更新
	    y = rand() % 9 + 1;
		if (arr[x][y] == '0')
		{
			arr[x][y] = '1';
			count --;
		}
	}
}
//计算周围八个位置雷的个数的函数
int mine_count(char arr[ROWS][COLS],int row,int col)
{
	int i = 0;
	int j = 0;
	int all = 0;
	for (i = -1; i <= 1 ; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			all = all + arr[row + i][col + j];//这里也加了自身
		}
	}
	return all - 9 * '0';   //因为前面加了自身，所以这里得多减一个  '0'
}
//判断是否展开函数
int is_spread(char mine[ROWS][COLS],int row,int col)
{
	int i = 0;
	int j = 0;
	int num = 0;
	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			if (mine[row+i][col+j] == '0')
			{
				num = num + 1;
			}
		}
	}
	if (9 == num)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
//展开函数   
void spread(char show[ROWS][COLS],int row,int col)
{
	int i = 0;
	int j = 0;
	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			show[row+i][col+j] = ' ';   //如果可以展开，就展开为空格
		}
	}

}
//void open(char mine[ROWS][COLS],char show[ROW][COL],int row ,int col)
//{
//	int i = 0;
//	int j = 0;
//	for (i = -1; i <= 1; i++)
//	{
//		for (j = -1; j <= 1; j++)
//		{
//			if (is_spread(mine,row,col) == 1)
//			{
//				spread(show,row,col);
//			}
//			else
//			{
//
//			}
//		}
//	}
//	for (i = -1; i <= 1; i++)
//	{
//		for (j = -1; j <= 1; j++)
//		{
//			open(mine, show, row + 1, col + 1);
//		}
//	}
//
//}
//寻找雷的函数
void find_mine(char arr_1[ROWS][COLS], char arr_2[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;
	//int num = 0;
	while (win != ROW*COL-MINE)
	{
		printf("请玩家输入坐标（坐标间以空格隔开）：");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
		{
			if (arr_1[x][y] == '1')               
			{
				printf("太遗憾了，你踩到雷了！\n");
				print_arr(arr_1, ROW, COL);
				break;
			}
			else   //arr_1[x][y] == '0'
			{
				if (is_spread(arr_1, x, y) == 1 )  //&&  arr_2[x][y] == '*'
				{
					spread(arr_2, x, y);
					win += 9;
					print_arr(arr_2, ROW, COL);
				}
				else     //防止玩家重复输入原坐标，导致win异常++
				{
					if (arr_2[x][y] == '*')
					{
						win++;
						//num = mine_count(arr_1, x, y);
						//arr_2[x][y] =  num + '0';  //一个数字加上字符0即可转化为相应数字的字符
						arr_2[x][y] = mine_count(arr_1, x, y) + '0';
						print_arr(arr_2, ROW, COL);
					}
					else
					{
						printf("重复输入，请重新输入\n");
					}
				}
				/*else
				{
					printf("重复输入，请重新输入\n");
				}*/
			}
		}
		else
		{
			printf("输入坐标违法，请重新输入\n");
		}
	}
}