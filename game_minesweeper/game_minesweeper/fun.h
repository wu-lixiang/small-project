#pragma once
#define ROW 9    //打印出棋盘的行
#define COL 9    //打印出棋盘的列
#define ROWS  ROW+2   //整个棋盘的总行
#define COLS  COL+2   //整个棋盘的总列
#define MINE 10       //设置雷的个数
#include<stdio.h>     
#include<stdlib.h>     //所引的头文件
#include<time.h>
void initial_arr(char arr[ROWS][COLS], int row, int  col,char set);   //初始化棋盘函数   
// 这里为声明  括号内都为形参，所以必须的定义类型
void print_arr(char arr[ROWS][COLS], int row, int col);        //打印棋盘函数
void get_mine(char arr[ROWS][COLS], int row, int col);      //设置雷的函数
void find_mine(char arr_1[ROWS][COLS], char arr_2[ROWS][COLS], int row, int col);   //查找雷的函数
