#define _CRT_SECURE_NO_WARNINGS
#include "fun.h"
void title()
{
	printf("**********************************\n");
	printf("**********************************\n");
	printf("********开始游戏---->> 1**********\n");
	printf("********退出游戏---->> 0**********\n");
	printf("**********************************\n");
	printf("**********************************\n");
}
void game()
{
	char mine[ROWS][COLS];
	char show[ROWS][COLS];
	initial_arr(mine, ROWS, COLS, '0'); //初始化放置雷的棋盘
	initial_arr(show, ROWS, COLS, '*');//初始化打印出的棋盘
	
	print_arr(show, ROW, COL);  //打印棋盘

	get_mine(mine, ROWS, COLS);//设置雷
	//print_arr(mine, ROW, COL);

	find_mine(mine, show, ROWS, COLS); //寻找雷
	
}
int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		title();
		scanf("%d", &input);
		
		switch (input)
		{
		case 1:
			printf("---------游戏开始！---------------\n");
			game();
			break;
		case 0:
			printf("退出游戏！\n");
			break;
		default:
			printf("选择错误，请重新选择!\n");
			break;
		}

	} while (input);

	return 0;
}