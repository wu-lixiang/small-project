#define _CRT_SECURE_NO_WARNINGS
#include "game.h"
////1 打印列表
void list()
{
	printf("**********************************\n");
	printf("**********************************\n");
	printf("*********开始游戏—> 1************\n");
	printf("*********退出游戏—> 0************\n");
	printf("**********************************\n");
	printf("**********************************\n");
}
//初始化棋盘
void init_chess(char arr[ROW][COL],int row,int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			arr[i][j] = ' ';   //单个字符要用单引号
		}
	}
}
//打印棋盘
void print_chess(char arr[ROW][COL],int row,int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (j < col - 1)
			{
				printf(" %c |", arr[i][j]);
			}
			else
			{
				printf(" %c  ", arr[i][j]);

			}
		}
		printf("\n");
		for (j = 0; j < col; j++)
		{
			printf("----");
		}
		printf("\n");
	}
}
//将玩家输入的值存储并放入棋盘内，
void game_chess(char arr[ROW][COL], int row, int col)
{
	while (1)
	{

		int x = 0;
		int y = 0;
		scanf("%d %d", &x, &y);// 1 11
		if ((x<1 || x>row) || (y<1 || y>col))//条件满足就进入
		{
			printf("坐标违法，请重新输入\n");
		}
		else
		{
			if (arr[x-1][y-1] == ' ')   //注意这里也要减
			{
				arr[x - 1][y - 1] = '#';  //优化为 √
				break;
			}
			else
			{
				printf("该位置已被占用，请重新输入坐标\n");
			}
		}
	}
}
//将电脑随机输入的值存储到棋盘内
void computer_chess(char arr[ROW][COL], int row, int col)   //优化方向：使电脑下的棋可以堵玩家的棋，或者自己连线
{
	int x = 0;
	int y = 0;
	while (1)
	{
		x = rand() % row;//范围为0~~9
		y = rand() % col;
		if (arr[x][y] == ' ')
		{
			arr[x][y] = '*';
			break;
		}
	}
}
//判断是玩家或电脑是否取得胜利
char win(char arr[ROW][COL], int row, int col)
{
	//判断行
	int i = 0;
	int j = 0;
	int num = 0;
	//int count = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col-1; j++)
		{

			if (arr[i][j] == arr[i][j + 1] && arr[i][j] != ' ')
			{
				num = num+1;
			}
			else
			{
				num = 0;  //只要中间隔了一次不相等，num就重新赋值为0
			}
			if (dragon - 1 == num)         //判断到底是几字成龙即可获胜  如果是五子成龙，则count等于四即可
			{
				return arr[i][j];
			}
		}
	}
	//判断列
	num = 0;  //此处要将num重新赋值为0 因为经过上面的代码后count的值已经发生变化
	for (i = 0; i < col; i++)
	{
		for (j = 0; j < row - 1; j++)
		{
			if (arr[j][i] == arr[j + 1][i] && arr[j][i] != ' ')
			{
				num = num + 1;
			}
			else
			{
				num = 0;
			}
			if (dragon - 1 == num)         //判断到底是几字成龙即可获胜  如果是五子成龙，则num等于四即可
			{
				return arr[j][i];          //返回arr[j][i] 而不是i  j
			}
		}
		
	}
	//判读对角线
	num = 0; //再次给num赋值
	for (i = 0,j = 0; i < row; i++,j++)
	{
		if (arr[i][j] == arr[i + 1][j + 1] && arr[i][j] != ' ')
		{
			num = num + 1;
		}
		else
		{
			num = 0;
		}
		if (dragon - 1 == num)
		{
			return arr[i][j];
		}
	}
	num = 0;  //再次给num重新赋值
	for (i = 0,j = col-1 ; i <row&&j>=0; i++,j--)
	{
		if (arr[i][j] == arr[i + 1][j - 1] && arr[i][j] != ' ')
		{
			num = num + 1;
		}
		else
		{
			num = 0;
		}
		if (dragon - 1 == num)
		{
			return arr[i][j];
		}
	}

	//判断是平局还是继续  
	num = 0;//再次给count重新赋值
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (arr[i][j] == ' ')  //为空格就进入条件
			{
				num = 1;
			}
		}
		
	}
	if (num == 0)            //是判断不是赋值
	{
		return 'E';    //返回就意味着平局
	}
	else
	{
		return 'C';   //返回就意味着继续游戏
	}
}