#pragma once
#include<string.h>
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define ROW 10  //这里定义的符号常量不用加分号在后面
#define COL 10  //这里定义的是表格的行和列
#define dragon 5   //这里定义的是几字成龙即可获胜
void list();//打印列表
//初始化棋盘
void init_chess(char arr[ROW][COL],int row ,int col);//二维数组作为形参，必须得写行 同时将行列传过来
void print_chess(char arr[ROW][COL],int row,int col);//打印棋盘
void game_chess(char arr[ROW][COL], int row, int col);//将玩家输入的值存储并放入棋盘内
void computer_chess(char arr[ROW][COL], int row, int col);//将电脑随机输入的值存储到棋盘内
char win(char arr[ROW][COL], int row, int col);//判断游戏状态，输、赢、平局、继续