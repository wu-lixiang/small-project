#define _CRT_SECURE_NO_WARNINGS

#include "game.h"
//主函数开始五子棋游戏（多子棋游戏）
int main()
{
	srand((unsigned int)time(NULL));//rand函数调用前要调用srand函数
	int input = 0;//这里的input不能放在do while循环里面
	char arr[ROW][COL] = { 0 };
	int row = ROW;
	int col = COL;
	char final = 0;
	do
	{
		list();//打印列表函数
		printf("输入 1 开始游戏  输入 0 退出游戏\n");
		printf("请输入一个整数：");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("退出游戏\n");
			break;
		case 1:
			printf("游戏开始\n");
			init_chess(arr, row, col);//初始化棋盘
			print_chess(arr, row, col);//打印棋盘
			while (1)
			{
				//提示玩家输入
				printf("玩家请输入坐标：\n");

				//将玩家输入的值存储并放入棋盘内

				game_chess(arr, row, col);
			
				//判断输、赢、平局、继续

				final =  win(arr, row, col);
				if (final != 'C')
				{
					if (final == '#')
					{
						printf("玩家获胜\n");
						print_chess(arr, row, col);
					}
					else if (final == '*')
					{
						printf("电脑获胜\n");
						print_chess(arr, row, col);
					}
					else
					{
						printf("平局\n");
						print_chess(arr, row, col);
					}
					break;
				}
				//将输入后的棋盘打印

				print_chess(arr, row, col);

				//提示玩家电脑输入的结果

				printf("电脑输入后：\n");

				//将电脑随机输入的值存储到棋盘内

				computer_chess(arr, row, col);

				
				//判断输、赢、平局、继续

				final = win(arr, row, col);
			    if (final != 'C')
				 {
					 if (final == '#')
					 {
						 printf("玩家获胜\n");
						 print_chess(arr, row, col);
					 }
					 else if (final == '*')
					 {
						 printf("电脑获胜\n");
						 print_chess(arr, row, col);
					 }
					 else
					 {
						 printf("平局\n");
						 print_chess(arr, row, col);
					 }
					 break;
				 }

				//将输入后的棋盘打印

				print_chess(arr, row, col);

			}
			break;
		 default:
			printf("输入错误，请重新输入\n");
		}
	} while(input);
	return 0;
}